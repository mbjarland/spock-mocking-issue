# Spock framework mock object issue

## What is this?
This is a mini project showcasing an issue with the [spock framework](http://spockframework.github.io/spock/docs/1.0/index.html)
([github](https://github.com/spockframework/spock)) mocking functionality. 

We have tried to the best of our knowledge to distill down the issue to the smallest possible 
project, but who knows, perhaps this is a symptom of something even simpler. 

## Environment
The project has been tested under:

    ------------------------------------------------------------
    Gradle 2.11
    ------------------------------------------------------------

    Build time:   2016-02-08 07:59:16 UTC
    Build number: none
    Revision:     584db1c7c90bdd1de1d1c4c51271c665bfcba978

    Groovy:       2.4.4
    Ant:          Apache Ant(TM) version 1.9.3 compiled on December 23 2013
    JVM:          1.6.0_45 (Sun Microsystems Inc. 20.45-b01)
    OS:           Linux 4.2.0-16-generic amd64

and with the following dependencies: 

    dependencies {
      compile      'org.codehaus.groovy:groovy:2.4.3'

      testCompile  'junit:junit:4.+', 
                   'cglib:cglib-nodep:2.2'
      testCompile( 'org.spockframework:spock-core:1.0-groovy-2.4' )
    }


## Running the project
To exhibit the error, run: 

    $ ./gradlew

the gradle build file has been configured with default tasks 'clean, build', so the above will 
compile the code and run the tests which should result in a failure. 

## Problem
We have the following setup: (see [the specification class](https://bitbucket.org/mbjarland/spock-mocking-issue/src/c31661de54ab7db9d206fde1ce397b98f7303caa/src/test/groovy/org/bobba/FailingSpockMockingSpecification.groovy?at=master&fileviewer=file-view-default) for details)

  * We have a ServiceSubClass with a method 'method(HttpRequest p)'
  * ServiceSubClass has a base class ServiceBaseClass which also has method 'method(HttpRequest p)'.    
  * The _parameter class_ HttpRequest inherits from class HttpRequestBase
  * HttpRequestBase implements interface HttpRequestInterface (we made this all up to make 
    things easier to grok, the class/interface names are arbitrary)
  * ServiceBaseClass also implements an overloaded method 'method(HttpRequestInterface p)'
  
given this setup and the following code: 

```
#!groovy
def "Mocking should work with sub class service call"() {
  setup: 
    HttpRequest p = Mock()
    def service = new ServiceSubClass()

  when: 
    def answer = service.method(p)

  then: "expect the sub class service method to be called, not base class"
    answer.tokenize("|").first().trim() == "sub class method(HttpRequest p)"
}
```
we would expect the service sub class method to be called as the method is overloaded in three places: 

* ServiceBaseClass.method(HttpRequestInterface p) // should not be called 
* ServiceBaseClass.method(HttpRequest p) // shadowed by sub class override
* ServiceSubClass.method(HttpRequest p)  // should be called 

but instead the base class interface method is called, i.e. the above test results in: 

    -- Mocking should work with sub class service call
    Condition not satisfied:

    answer.tokenize("|").first().trim() == "sub class method(HttpRequest p)"
    |      |             |       |      |
    |      |             |       |      false
    |      |             |       |      13 differences (68% similarity)
    |      |             |       |      (ba)s(e-) class method(HttpRequest(Interface) p)
    |      |             |       |      (--)s(ub) class method(HttpRequest(---------) p)
    |      |             |       base class method(HttpRequestInterface p)
    |      |             base class method(HttpRequestInterface p) 
    |      [base class method(HttpRequestInterface p) ,  called with Mock for type 'HttpRequest' named 'p' -> null]
    base class method(HttpRequestInterface p) | called with Mock for type 'HttpRequest' named 'p' -> null

      at org.bobba.FailingSpockMockingSpecification.Mocking should work with sub class service call(FailingSpockMockingSpecification.groovy:15)


