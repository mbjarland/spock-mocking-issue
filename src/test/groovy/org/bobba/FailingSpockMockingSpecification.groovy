package org.bobba

import spock.lang.*

class FailingSpockMockingSpecification extends Specification {
  def "Mocking should work with sub class service call"() {
    setup: 
      HttpRequest p = Mock()
      def service = new ServiceSubClass()

    when: 
      def answer = service.method(p)

    then: "expect the sub class service method to be called, not base class"
      answer.tokenize("|").first().trim() == "sub class method(HttpRequest p)"
  }

  def "Mocking should work with sub class service call and java casts"() {
    setup: 
      HttpRequest p = Mock()
      def service = new ServiceSubClass()

    when: 
      def answer = service.method((HttpRequest) p)

    then: "expect the sub class service method to be called, not base class"
      answer.tokenize("|").first().trim() == "sub class method(HttpRequest p)"
  }

  def "Mocking should work with sub class service call and groovy 'as' cast"() {
    setup: 
      HttpRequest p = Mock()
      def service = new ServiceSubClass()

    when: 
      def answer = service.method(p as HttpRequest)

    then: "expect the sub class service method to be called, not base class"
      answer.tokenize("|").first().trim() == "sub class method(HttpRequest p)"
  }
}

class ServiceBaseClass {
  String method(HttpRequestInterface p) {
    "base class method(HttpRequestInterface p) | called with ${p} -> ${p.value}"
  }

  String method(HttpRequest p) {
    "base class method(HttpRequest p) | called with ${p} -> ${p.value}"
  }
}

class ServiceSubClass extends ServiceBaseClass {
  @Override
  String method(HttpRequest p) {
    "sub class method(HttpRequest p) | called with ${p} -> ${p.value}"
  }
}

interface HttpRequestInterface {
  String getValue()
}

class HttpRequestBase implements HttpRequestInterface {
  public String getValue() {
    "<http base value>"
  }
}

class HttpRequest extends HttpRequestBase {
  public String getValue() {
    "<http sub value>"
  }
}


